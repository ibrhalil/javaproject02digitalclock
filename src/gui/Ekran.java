package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.LocalTime;

public class Ekran extends JFrame implements ActionListener
{
    private JPanel panel1;
    private JLabel labelSaat;
    private JLabel labelDakika;
    private JLabel ikiNokta;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;

    Toolkit toolkit;
    Dimension dimension;

    int ekranGenislik;
    int ekranYukseklik;
    int ortaNoktaX;
    int ortaNoktaY;

    private Timer zaman = new Timer(499,this);
    LocalTime localTime = LocalTime.now();

    public Ekran()
    {
        toolkit = panel1.getToolkit();
        dimension = toolkit.getScreenSize();

        ekranGenislik = (int) (dimension.width*0.15);//dimension.width/2;
        ekranYukseklik =(int) (dimension.width*0.10);//dimension.height/3;

        ortaNoktaX = dimension.width/2 - ekranGenislik/2;
        ortaNoktaY = dimension.height/2 - ekranYukseklik/2;

        setBounds(ortaNoktaX, ortaNoktaY, ekranGenislik, ekranYukseklik);

        setUndecorated(true);
        setBackground(new Color(1.0f,1.0f,1.0f,0.0f));

        button1.addActionListener(this);
        button2.addActionListener(this);
        button3.addActionListener(this);
        button4.addActionListener(this);
        button5.addActionListener(this);
        button6.addActionListener(this);
        ikiNokta.addMouseListener(new MouseAdapter()
        {
            @Override public void mouseClicked(MouseEvent e)
            {
                super.mouseClicked(e);
                konumlandir(dimension.width/2 - ekranGenislik/2, dimension.height/2 - ekranYukseklik/2);
            }
        });

        saatYaz();

        zaman.start();

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        add(panel1);
    }

    @Override public void actionPerformed(ActionEvent e)
    {
       saatYaz();

       if(e.getSource().equals(button1))
       {
           konumlandir(ortaNoktaX,0);
       }
       else if(e.getSource().equals(button2))
       {
           konumlandir(dimension.width-ekranGenislik,ortaNoktaY);
       }
       else if(e.getSource().equals(button3))
       {
           konumlandir(0,ortaNoktaY);
       }
       else if(e.getSource().equals(button4))
       {
           konumlandir(ortaNoktaX,dimension.height-ekranYukseklik);
       }
       else if(e.getSource().equals(button5))
       {
           int w = ekranGenislik-10;
           int h = ekranYukseklik-10;
           boyutlandir(w,h);
       }
       else if(e.getSource().equals(button6))
       {
           int w = ekranGenislik+10;
           int h = ekranYukseklik+10;
           boyutlandir(w,h);
       }
    }

    void saatYaz()
    {
        localTime = LocalTime.now();
        int saat = localTime.getHour(), dk = localTime.getMinute();

        labelSaat.setText(saat > 9 ? saat+"":"0"+saat);
        labelDakika.setText(dk > 9 ? dk+"":"0"+dk);

        if(localTime.getSecond() % 2 ==0)
        {
            ikiNokta.setForeground(new Color(60,63,65));
        }
        else
        {
            ikiNokta.setForeground(new Color(255,167,75));
        }
    }

    void konumlandir(int x, int y)
    {
        ortaNoktaX = x;
        ortaNoktaY = y;
        setBounds(ortaNoktaX,ortaNoktaY,ekranGenislik,ekranYukseklik);
    }

    void boyutlandir(int genislik,int yukseklik)
    {
        if(genislik>dimension.width/2 ||  yukseklik>dimension.height/2 || yukseklik<dimension.height/7)
        {
            return;
        }
        else
        {
            labelSaat.setFont(new Font("Consolas",Font.PLAIN,yukseklik/5));
            labelDakika.setFont(new Font("Consolas",Font.PLAIN,yukseklik/5));
            ikiNokta.setFont(new Font("Consolas",Font.PLAIN,yukseklik/5));

            ekranGenislik = genislik;
            ekranYukseklik = yukseklik;

            setBounds(ortaNoktaX,ortaNoktaY,ekranGenislik,ekranYukseklik);
            konumlandir(ortaNoktaX,ortaNoktaY);
        }

    }
}
